package com.cise.lire.ParallelSearcher.utils;

import java.io.File;
import java.util.ArrayList;

import com.cise.lire.ParallelSearcher.utils.IResult;
import com.cise.utils.Util;

/**
 * 搜索器搜索返回的结果类
 * 
 */
public class Result extends IResult{
	private final static String pre = Util.PICTURE_PATH;
	/**
	 * 图片 Path
	 */
	private String path;
	
	public Result(double dis,String url){
		this.distance = dis;
		this.path = url;
	}

	/**
	 * 判断两个对象是否相同
	 */
	@Override
	public boolean equal(IResult r2) {
		if(path!=null && path.length()!=0){
			return path.equals(((Result)r2).path);
		}
		return false;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "Result [url=" + path + ", distance=" + distance + "]";
	}

}
