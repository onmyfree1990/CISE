package com.cise.lire.ParallelSearcher.utils;

import java.util.ArrayList;

/**
 * 结果的抽象类
 * @author 文友
 *
 */
public abstract class IResult {
	protected double distance;
	
	public abstract boolean equal(IResult r2);
	
	/**
	 * 查找该对象在集合中的位置
	 */
	public int indexInList(ArrayList<IResult> results) {
		// TODO Auto-generated method stub
		for(int i = 0;i < results.size();i++){
			if(equal(results.get(i))){
				return i;
			}
		}
		return -1;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public double getDistance() {
		return distance;
	}
	
}
