package com.cise.lire.ParallelSearcher;

import com.cise.lire.ParallelSearcher.sort.RankSort;

/**
 * 基于RankSort排序算法的并行搜索器
 * @author 文友
 *
 */
public class RankParallelSearcher extends AbstractParallelSearcher {

	public RankParallelSearcher() {
		this.sort = new RankSort();
	}
}
