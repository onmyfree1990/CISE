package com.cise.lire.ParallelSearcher;

import java.util.ArrayList;
import java.util.List;

import com.cise.utils.Util;

import net.semanticmetadata.lire.ImageSearcher;

/**
 * 并行搜索器工厂
 * @author 文友
 *
 */
public class ParallelSearcherFactory {
	
	/**
	 * 据所配置的排序算法构建并行搜索器
	 * @param algorithms
	 * @return
	 */
	public static AbstractParallelSearcher createParallelSearcher(List<String> algorithms){
		switch(Util.getSortName()){
		case Util.RankSortName:
			return createRankParallelSearcher(algorithms);
		case Util.RatioSortName:
			return createRatioParallelSearcher(algorithms);
		default:
			return createDefaultParallelSearcher(algorithms);
		}
	}
	/**
	 * 创建基于RankSort排序算法的并行搜索器
	 * @param searchers
	 * @return AbstractParallelSearcher
	 */
	public static AbstractParallelSearcher createRankParallelSearcher(List<String> algorithms){
		AbstractParallelSearcher aps = new RankParallelSearcher();
		aps.setIndexPaths(algorithms);
		return aps;
	}
	
	/**
	 * 创建默认并行搜索器
	 * @param searchers
	 * @return AbstractParallelSearcher
	 */
	public static AbstractParallelSearcher createDefaultParallelSearcher(List<String> algorithms){
		return createRankParallelSearcher(algorithms);
	}
	
	/**
	 * 创建基于RatioSort排序算法的并行搜索器
	 * @param searchers
	 * @return AbstractParallelSearcher
	 */
	public static AbstractParallelSearcher createRatioParallelSearcher(List<String> algorithms){
		AbstractParallelSearcher aps = new RatioParallelSearcher();
		aps.setIndexPaths(algorithms);
		return aps;
	}
}
