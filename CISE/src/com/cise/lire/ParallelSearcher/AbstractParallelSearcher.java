package com.cise.lire.ParallelSearcher;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.lucene.index.IndexReader;

import com.cise.lire.ParallelSearcher.sort.ISort;
import com.cise.lire.ParallelSearcher.utils.IResult;
import com.cise.utils.Util;

/**
 * 并行搜索器的虚基类
 * 
 * @author 文友
 *
 */
public abstract class AbstractParallelSearcher implements SearcherThread.CallBack{
	/**
	 * 搜索器所对应的索引目录<br>
	 * 该变量正常由特征提取算法名列表赋值<br>
	 * 因为索引目录名就是由算法名命名的
	 */
	protected List<String> indexPaths = null;
	/**
	 * 排序算法
	 */
	protected ISort sort = null;
	/**
	 * 各个搜索器的搜索结果
	 */
	private ArrayList<IResult[]> results = new ArrayList<IResult[]>();
	/**
	 * 倒计数的锁存器<br>
	 * 此处不使用线程池的 await 方法是因为该线程池出于逻辑需求不能调用 shutdown 方法<br>
	 * 没有调用 shutdown 方法，线程池就会无限等待下去
	 */
	private CountDownLatch count;
	/**
	 * 管理搜索过程中创建的所有线程的线程池
	 */
	private ExecutorService pool = Executors.newFixedThreadPool(Util.THREAD_POOL_MAX_SIZE);

	/**
	 * 搜索
	 * 
	 * @param img
	 * @param ir
	 * @return
	 */
	public ArrayList<IResult> search(BufferedImage img) {
		// 获取已生成好的索引文件
		IndexReader ir;
		try {
			if (this.sort == null || img == null || null == this.indexPaths || this.indexPaths.size() == 0) {
				return null;
			} else {
				this.count = new CountDownLatch(this.indexPaths.size());
				System.out.println("开始搜索时间：" + new Date(System.currentTimeMillis()));
				// 每一个搜索器占用一个线程，执行各自的搜索
				for (int i = 0; i < this.indexPaths.size(); i++) {
					SearcherThread searcherThread = new SearcherThread(img, 
							Util.INDEX_ROOT_PATH + this.indexPaths.get(i) + File.separator, 
							this.pool, this.indexPaths.get(i), this);
					this.pool.execute(searcherThread);
				}
				// 阻塞主线程
				this.count.await();
				// 因为 pool 也需要管理 SearcherThread 所以需要所有线程运行完后才关闭线程池
				this.pool.shutdown();
				System.out.println("搜索结束时间：" + new Date(System.currentTimeMillis()));
				// 所有线程执行完后，主线程继续后续操作
				// 排序并返回结果
				return this.sort.sort(results);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		return null;
	}

	public List<String> getIndexPaths() {
		return this.indexPaths;
	}

	public void setIndexPaths(List<String> indexPaths) {
		this.indexPaths = indexPaths;
	}

	@Override
	public void setResult(IResult[] iResults) {
		this.results.add(iResults);
		this.count.countDown();
	}
	
}
