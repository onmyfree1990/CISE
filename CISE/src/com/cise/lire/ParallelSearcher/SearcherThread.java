package com.cise.lire.ParallelSearcher;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.IOContext;
import org.apache.lucene.store.IndexInput;
import org.apache.lucene.store.IndexOutput;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;

import com.cise.lire.ParallelSearcher.utils.IResult;
import com.cise.lire.ParallelSearcher.utils.Result;
import com.cise.utils.Util;

public class SearcherThread extends Thread {
	/**
	 * 搜索目标图片<br>
	 * 即用户提交的图片
	 */
	private BufferedImage image;
	/**
	 * 索引根目录
	 */
	private String rootIndexPath;
	/**
	 * 该线程所在的线程池<br>
	 * 将该线程的子线程也交给该线程所在的线程池中管理
	 */
	private ExecutorService pool;
	/**
	 * 特征提取算法名
	 */
	private String algorithmName;
	/**
	 * 搜索結果
	 */
	private List<IResult> results;
	/**
	 * 回调对象
	 */
	private CallBack callBack;
	/**
	 * 倒计数的锁存器<br>
	 * 用来让主线程等待子线程执行完毕再继续运行<br>
	 * 该方法更适用于直接在代码里重写 Thread
	 */
	private CountDownLatch count;
	
	public SearcherThread(BufferedImage image, String rootIndexPath, 
			ExecutorService pool, String algorithmName, 
			CallBack callBack) {
		this.image = image;
		this.rootIndexPath = rootIndexPath;
		this.pool = pool;
		this.algorithmName = algorithmName;
		this.results = new LinkedList<IResult>();
		this.callBack = callBack;
	}

	@Override
	public void run() {
		// 从索引根目录中获取所有的子目录
		File file = new File(this.rootIndexPath);
		if(!file.exists()) {
			this.callBack.setResult(null);
			return;
		}
		File[] subFiles = file.listFiles();
		if(null == subFiles || subFiles.length == 0) {
			this.callBack.setResult(null);
			return;
		}
		this.count = new CountDownLatch(subFiles.length);
		System.out.println(this.algorithmName + " 线程开始时间：" + new Date(System.currentTimeMillis()));
		
		// 为每个子目录创建一个新的进程去搜索索引
		for(int i = 0; i < subFiles.length; i++){
			final File f = subFiles[i];
			this.pool.execute(new Thread(){
				@Override
				public void run(){
					try {
						System.out.println("searching the index file : " + f.getAbsolutePath());
						System.out.println(this + " " + algorithmName + " " + f.getName() + " 线程开始时间：" + new Date(System.currentTimeMillis()));
						IndexReader ir = DirectoryReader.open(FSDirectory.open(f));
						ImageSearcher searcher = Util.getSearcherByAlgorithmName(algorithmName);
						ImageSearchHits hits = searcher.search(image, ir);
						for(int i = 0; i < hits.length(); i++){
							String fileName = hits.doc(i).getValues(
											DocumentBuilder.FIELD_NAME_IDENTIFIER)[0];
							synchronized (results) {
								results.add(new Result(hits.score(i),
										fileName));
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					} finally{
						System.out.println(algorithmName + " " + f.getName() + " 线程结束时间：" + new Date(System.currentTimeMillis()));
						count.countDown();
					}
				}
			});
		}
		
		// 综合所有搜索结果
		try {
			this.count.await();
			System.out.println(this + " " + algorithmName + " 线程结束时间：" + new Date(System.currentTimeMillis()));
			IResult[] iResult = new IResult[results.size()];
			for(int i = 0; i < results.size(); i++){
				iResult[i] = results.get(i);
			}
			this.callBack.setResult(iResult);
		} catch (InterruptedException e) {
			this.callBack.setResult(null);
			e.printStackTrace();
		}
	}
	
	/**
	 * 搜索线程的回调函数<br>
	 * 用来设置搜索结果
	 * @author Mutear
	 *
	 */
	public interface CallBack{
		public void setResult(IResult[] iResults);
	}

}
