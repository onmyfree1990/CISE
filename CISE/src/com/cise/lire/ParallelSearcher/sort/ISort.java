package com.cise.lire.ParallelSearcher.sort;

import java.util.ArrayList;

import com.cise.lire.ParallelSearcher.utils.IResult;

/**
 * 排序算法的接口
 * @author 文友
 *
 */
public interface ISort {
	public ArrayList<IResult> sort(ArrayList<IResult[]> results);
}
