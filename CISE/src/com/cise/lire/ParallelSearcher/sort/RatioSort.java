package com.cise.lire.ParallelSearcher.sort;

import java.util.ArrayList;
import java.util.Collections;

import com.cise.lire.ParallelSearcher.utils.IResult;

/**
 * 比例排序</br>
 * average为各个搜索器的距离均值</br>
 * 搜索结果的新距离为距离distance/average</br>
 * 当不同搜索结果列表中的两个结果相同</br>
 * 设原距离为w、新加距离为w2，则新距离为w-((w-(int)w)*(w2-(int)w2))</br>
 * 经粗略计算距离的域为 [0, 5]
 * @author 文友
 *
 */
public class RatioSort implements ISort {

	public ArrayList<IResult> sort(ArrayList<IResult[]> results) {
		if(results == null) return null;
		ArrayList<IResult> result = new ArrayList<IResult>();
		ArrayList<Double> distance = new ArrayList<Double>();
		ArrayList<Double> average = new ArrayList<Double>();
		
		//求得均值
		for(int i = 0;i < results.size();i++){
			IResult[] iResult = results.get(i);
			if(iResult == null){
				average.add(0.0);
			}
			double sum = 0;
			for(int j = 0;j < iResult.length;j++){
				sum += iResult[j].getDistance();
			}
			average.add(sum/iResult.length);
		}
		
		//求距离
		for(int i = 0;i < results.size();i++){
			IResult[] iResult = results.get(i);
			if(iResult == null) continue;
			for(int j = 0;j < iResult.length;j++){
				//如果当前结果未添加进结果列表中
				int index = iResult[j].indexInList(result);
				if(index < 0){
					result.add(iResult[j]);
					distance.add(iResult[j].getDistance()/average.get(i));
				}
				//如果已添加
				else{
					double w = distance.get(index);
					double w2 = iResult[j].getDistance()/average.get(i);
					if(w > w2){
						double t = w;
						w = w2;
						w2 = t;
					}
					// Math.pow(2, -1*w2) + 1 的域为 (1, 2]
					// 因此能达到倍减的效果
					distance.set(index, w / (Math.pow(2, -1*w2) + 1));
				}
			}
		}

		//将新的距离作为结果的距离
		for(int i = 0; i < result.size(); i++){
			result.get(i).setDistance(distance.get(i));
		}
		
		return sortWithWeight(result);
	}

	/**
	 * 根据距离排序
	 * 以下使用堆排序
	 * @param result
	 * @param distances
	 * @return
	 */
	private ArrayList<IResult> sortWithWeight(ArrayList<IResult> result){
		int size = result.size();
		for(int i = 0;i < size;i++){
			//还需要排序的节点数
			int s = size-i;
			for(int j = s/2-1;j >= 0;j--){
				//左右子树中距离较大的若为左子树，则max等于0（默认），若为右子树则max则为1
				int max = 0;
				if(2*j+2<s && result.get(2*j+1).getDistance()<result.get(2*j+2).getDistance()){
					max = 1;
				}
				if(max == 0){
					//父节点的距离小于左子树距离
					if(result.get(j).getDistance() < result.get(2*j+1).getDistance()){
						Collections.swap(result, j, 2*j+1);
					}
				}
				else{
					//父节点的距离小于右子树距离
					if(result.get(j).getDistance() < result.get(2*j+2).getDistance()){
						Collections.swap(result, j, 2*j+2);
					}
				}
			}
			//堆顶为未排序的节点中的最大值
			//堆顶与未排序的最后一个节点互换
			Collections.swap(result, 0, s-1);
		}
		return result;
	}

}
