package com.cise.lire.ParallelSearcher.sort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import com.cise.lire.ParallelSearcher.utils.IResult;

/**
 * 将图片在单个搜索器的搜索结果中的排名作为新距离</br>
 * 不同搜索结果列表中的相同的两张图片</br>
 * 将距离设置为两距离平均 最后根据每张图片的距离进行排序
 * 
 * @author 文友
 *
 */
public class RankSort implements ISort {

	public ArrayList<IResult> sort(ArrayList<IResult[]> results) {
		if(results == null) return null;
		ArrayList<IResult> result = new ArrayList<IResult>();
		ArrayList<Integer> distances = new ArrayList<Integer>();

		// 遍历所有结果
		for (int i = 0; i < results.size(); i++) {
			IResult[] iResults = results.get(i);
			if(iResults == null) continue;
			for (int j = 0; j < iResults.length; j++) {
				// 如果当前结果未添加进结果列表中
				int index = iResults[j].indexInList(result);
				if (index < 0) {
					result.add(iResults[j]);
					distances.add(j);
				}
				// 如果已添加
				else {
					int weight = distances.get(index);
					distances.set(index, (weight + j) / 2);
				}
			}
		}

		// 将新的距离作为结果的距离
		for (int i = 0; i < result.size(); i++) {
			result.get(i).setDistance(distances.get(i));
		}

		return sortWithWeight(result);
	}

	/**
	 * 根据距离排序 以下使用堆排序
	 * 
	 * @param result
	 * @param distances
	 * @return
	 */
	private ArrayList<IResult> sortWithWeight(ArrayList<IResult> result) {
		int size = result.size();
		for (int i = 0; i < size; i++) {
			// 还需要排序的节点数
			int s = size - i;
			for (int j = s / 2 - 1; j >= 0; j--) {
				// 左右子树中距离较大的若为左子树，则max等于0（默认），若为右子树则max则为1
				int max = 0;
				if (2 * j + 2 < s
						&& result.get(2 * j + 1).getDistance() < result.get(2 * j + 2).getDistance()) {
					max = 1;
				}
				if (max == 0) {
					// 父节点的距离小于左子树距离
					if (result.get(j).getDistance() < result.get(2 * j + 1).getDistance()) {
						Collections.swap(result, j, 2 * j + 1);
					}
				} else {
					// 父节点的距离小于右子树距离
					if (result.get(j).getDistance() < result.get(2 * j + 2).getDistance()) {
						Collections.swap(result, j, 2 * j + 2);
					}
				}
			}
			// 堆顶为未排序的节点中的最大值
			// 堆顶与未排序的最后一个节点互换
			Collections.swap(result, 0, s - 1);
		}
		return result;
	}

}
