package com.cise.lire.ParallelSearcher;

import com.cise.lire.ParallelSearcher.sort.RatioSort;

/**
 * 基于RatioSort排序算法的并行搜索器
 * @author 文友
 *
 */
public class RatioParallelSearcher extends AbstractParallelSearcher {
	public RatioParallelSearcher(){
		this.sort = new RatioSort();
	}
}
