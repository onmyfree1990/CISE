package com.cise.lire.Indexer;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.utils.FileUtils;
import net.semanticmetadata.lire.utils.LuceneUtils;

import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;

import com.cise.utils.Util;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.io.OutputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 索引器
 */
public class Indexer {
	/**
	 * 基于配置信息中的图片库地址，添加后缀地址，启动索引器
	 * @param sufPath
	 */
	public static void start(String sufPath) {
		String indexPath = null;
		if (null != sufPath || sufPath.length() != 0)
			indexPath = Util.PICTURE_PATH + sufPath + File.separator;
		else{
			indexPath = Util.PICTURE_PATH;
		}
		System.out.println("Indexing images in " + indexPath);

		// Getting all images from a directory and its sub directories.
		ArrayList<String> images = null;
		try {
			images = FileUtils.getAllImages(new File(indexPath), true);
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		for (Iterator i = Util.ALGORITHM_NAMES.iterator(); i.hasNext();) {
			final String algorithmName = (String) i.next();
			final ArrayList<String> imgs = images;
			new Thread() {
				@Override
				public void run() {
					// Creating a CEDD document builder and indexing all
					// files.
					DocumentBuilder builder = Util
							.getBuilderByAlgorithmName(algorithmName);
					// Creating an Lucene IndexWriter
					IndexWriterConfig conf = new IndexWriterConfig(
							LuceneUtils.LUCENE_VERSION, new WhitespaceAnalyzer(
									LuceneUtils.LUCENE_VERSION));
					String indexPath = Util.INDEX_ROOT_PATH + algorithmName
							+ File.separator;
					File file = new File(indexPath);
					if (!file.exists()) {
						file.mkdirs();
						indexPath += 0;
					} else {
						int num = new File(indexPath).listFiles().length;
						indexPath += num;
					}
					IndexWriter iw;
					try {
						iw = new IndexWriter(FSDirectory.open(new File(
								indexPath)), conf);
						// Iterating through images building the low level
						// features
						for (Iterator<String> it = imgs.iterator(); it
								.hasNext();) {
							String imageFilePath = it.next();
							System.out.println(algorithmName + " Indexing "
									+ imageFilePath);
							try {
								BufferedImage img = ImageIO
										.read(new FileInputStream(imageFilePath));
								if (null == img)
									continue;
								Document document = builder.createDocument(img,
										imageFilePath);
								iw.addDocument(document);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						// closing the IndexWriter
						iw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}.start();
		}
	}
	
	/**
	 * 基于配置信息中的图片库地址，启动索引器
	 */
	public static void start(){
		start(null);
	}
}
