package com.cise.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.DocumentBuilderFactory;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.ImageSearcherFactory;

/**
 * 工具类
 * 
 * @author 文友
 *
 */
public class Util {
	/**
	 * 项目根目录
	 */
	public static String ROOT_PATH;
	/**
	 * 项目根目录
	 */
	public final static String BASE_PATH = "basePath";
	/**
	 * 基础配置文件名
	 */
	public final static String BASE_CONFIG = "baseConfig.properties";
	/**
	 * 每种 ImageSearcher 搜索结果的数量
	 */
	public static int EACH_SEARCHER_RESULT_NUM;
	/**
	 * 每种 ImageSearcher 搜索结果数量的配置名
	 */
	public final static String EARCH_SEARCHER_RESULT_NUM_CONFIG_NAME = "eachSearcherResultNum";
	/**
	 * 线程池最大线程数
	 */
	public static int THREAD_POOL_MAX_SIZE;
	/**
	 * 线程池最大线程数的配置名
	 */
	public final static String THREAD_POOL_MAX_SIZE_CONFIG_NAME = "threadPoolMaxSize";
	/**
	 * 特征提取算法配置文件名
	 */
	public final static String ALGORITHM_CONFIG = "algorithmConfig.properties";
	/**
	 * 选中的特征提取算法
	 */
	public static List<String> SELECTED_ALGORITHMS;
	/**
	 * 特征提取算法名
	 */
	public static Set<String> ALGORITHM_NAMES;
	/** 图片库目录 */
	public static String PICTURE_PATH;
	/** 图片库目录属性名 */
	public final static String PICTURE_PATH_ATT_NAME = "picturePath";
	/**
	 * 输入（上传）图片的存放目录
	 */
	public static String UPLOAD_PATH;

	static {
		// 读取基础配置文件
		InputStream inputStream = new Util().getClass().getClassLoader()
				.getResourceAsStream(BASE_CONFIG);
		// 加载成配置对象
		Properties p = new Properties();
		try {
			p.load(inputStream);
			// 获取配置信息
			THREAD_POOL_MAX_SIZE = Integer.parseInt(p
					.getProperty(THREAD_POOL_MAX_SIZE_CONFIG_NAME));
			EACH_SEARCHER_RESULT_NUM = Integer.parseInt(p
					.getProperty(EARCH_SEARCHER_RESULT_NUM_CONFIG_NAME));
			ROOT_PATH = p.getProperty(BASE_PATH);
			PICTURE_PATH = p.getProperty(PICTURE_PATH_ATT_NAME);
			UPLOAD_PATH = PICTURE_PATH + "upload" + File.separator;
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// 读取特征提取算法配置文件
		inputStream = new Util().getClass().getClassLoader()
				.getResourceAsStream(ALGORITHM_CONFIG);
		// 加载成配置对象
		p = new Properties();
		try {
			p.load(inputStream);
			SELECTED_ALGORITHMS = new ArrayList<String>();
			Set keys = p.keySet();
			ALGORITHM_NAMES = keys;
			for (Iterator it = keys.iterator(); it.hasNext();) {
				String key = (String) it.next();
				String value = p.getProperty(key);
				if (null != value && value.equalsIgnoreCase("true")) {
					SELECTED_ALGORITHMS.add(key);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 存放索引文件的根目录
	 */
	public static String INDEX_ROOT_PATH = ROOT_PATH + "index" + File.separator;

	/**
	 * JCD 算法名
	 */
	public final static String JCD_NAME = "jcd";
	/**
	 * CAC 算法名
	 */
	public final static String CAC_NAME = "cac";
	/**
	 * CEDD 算法名
	 */
	public final static String CEDD_NAME = "cedd";
	/**
	 * CH 算法名
	 */
	public final static String CH_NAME = "ch";
	/**
	 * CL 算法名
	 */
	public final static String CL_NAME = "cl";
	/**
	 * EH 算法名
	 */
	public final static String EH_NAME = "eh";
	/**
	 * FCTH 算法名
	 */
	public final static String FCTH_NAME = "fcth";
	/**
	 * Gabor 算法名
	 */
	public final static String GABOR_NAME = "gabor";
	/**
	 * Hasing CEDD 算法名
	 */
	public final static String HASHING_CEDD_NAME = "hcedd";
	/**
	 * JH 算法名
	 */
	public final static String JH_NAME = "jh";
	/**
	 * JCH 算法名
	 */
	public final static String JCH_NAME = "jch";
	/**
	 * LL 算法名
	 */
	public final static String LL_NAME = "ll";
	/**
	 * OH 算法名
	 */
	public final static String OH_NAME = "oh";
	/**
	 * PHOG 算法名
	 */
	public final static String PHOG_NAME = "phog";
	/**
	 * SCD 算法名
	 */
	public final static String SCD_NAME = "scd";
	/**
	 * Tamura 算法名
	 */
	public final static String TAMURA_NAME = "tamura";

	/**
	 * 爬虫时所需要的功能方法：将图片链接转换为能够作为文件地址的文件地址 如将/转换为\，将链接中的?转化为？，对链接进行编码
	 * 
	 * @param urlString
	 * @return
	 */
	public static String url2Path(String urlString) {
		String path = null;
		try {
			URL url = new URL(urlString);
			String host = url.getHost();
			String mPath = url.getPath();
			String mQuery = url.getQuery();
			String ref = url.getRef();

			path = host + splitPath(mPath);
			if (mQuery != null && mQuery.length() != 0) {
				int index = mQuery.indexOf("http://");
				if (index < 0) {
					path += "？" + splitPath(mQuery);
				} else {
					String path1 = splitPath(mQuery.substring(0, index));
					String path2 = url2Path(mQuery.substring(index));
					path += "？" + path1 + path2;
				}
			}
			if (null != ref && ref.length() != 0) {
				path += File.separator + "#" + ref;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return codeReserved(path);
	}

	/**
	 * 将链接中的/转换为\
	 * 
	 * @param urlPath
	 * @return
	 */
	private static String splitPath(String urlPath) {
		String path = "";
		String[] splitPath = urlPath.split("/");
		for (int i = 0; i < splitPath.length; i++) {
			if (splitPath[i].length() <= 0)
				continue;
			path += File.separator + splitPath[i];
		}
		return path;
	}

	/**
	 * 获取当前系统日期，格式为20150308
	 */
	public static String getDate() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		return formatter.format(date);
	}

	/**
	 * 对URL中保留字进行编码
	 */
	public static String codeReserved(String str) {
		str = str.replace('.', '%');
		str = str.replaceAll("%", "_2E");
		return str;
	}

	/**
	 * 恢复字符串中的URL保留字
	 */
	public static String reserve(String str) {
		str = str.replaceAll("_2E", ".");
		String[] strs = null;
		/**
		 * \\\\被java转换成\\,\\又被正则表达式转换成\。
		 */
		if (File.separator.equals("\\")) {
			strs = str.split("\\\\");
		} else {
			strs = str.split(File.separator);
		}
		str = strs[0];
		for (int i = 1; i < strs.length; i++) {
			str = str + "/" + strs[i];
		}
		str = "http://" + str;
		return str;
	}

	/**
	 * MD5加码 生成32位md5码
	 */
	public static String string2MD5(String inStr) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return "";
		}
		char[] charArray = inStr.toCharArray();
		byte[] byteArray = new byte[charArray.length];

		for (int i = 0; i < charArray.length; i++)
			byteArray[i] = (byte) charArray[i];
		byte[] md5Bytes = md5.digest(byteArray);
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < md5Bytes.length; i++) {
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16)
				hexValue.append("0");
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();

	}

	/**
	 * 加密解密算法 执行一次加密，两次解密
	 */
	public static String convertMD5(String inStr) {

		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ 't');
		}
		String s = new String(a);
		return s;
	}

	private static ImageSearcher cacIS = null;
	private static ImageSearcher ceddIS = null;
	private static ImageSearcher chIS = null;
	private static ImageSearcher clIS = null;
	private static ImageSearcher ehIS = null;
	private static ImageSearcher fcthIS = null;
	private static ImageSearcher gaborIS = null;
	private static ImageSearcher hceddIS = null;
	private static ImageSearcher jcdIS = null;
	private static ImageSearcher jchIS = null;
	private static ImageSearcher jhIS = null;
	private static ImageSearcher llIS = null;
	private static ImageSearcher ohIS = null;
	private static ImageSearcher phogIS = null;
	private static ImageSearcher scdIS = null;
	private static ImageSearcher tamuraIS = null;

	/**
	 * 根据特征提取算法名获取对应的 ImageSearcher <br>
	 * 单例模式
	 * 
	 * @param name
	 * @return
	 */
	public static ImageSearcher getSearcherByAlgorithmName(String name) {
		switch (name) {
		case Util.CAC_NAME:
			if (null == cacIS) {
				return ImageSearcherFactory
						.createAutoColorCorrelogramImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return cacIS;
			}
		case Util.CEDD_NAME:

			if (null == ceddIS) {
				return ImageSearcherFactory
						.createCEDDImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return ceddIS;
			}
		case Util.CH_NAME:
			if (null == chIS) {
				return ImageSearcherFactory
						.createColorHistogramImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return chIS;
			}
		case Util.CL_NAME:
			if (null == clIS) {
				return ImageSearcherFactory
						.createColorLayoutImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return clIS;
			}
		case Util.EH_NAME:
			if (null == ehIS) {
				return ImageSearcherFactory
						.createEdgeHistogramImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return ehIS;
			}
		case Util.FCTH_NAME:
			if (null == fcthIS) {
				return ImageSearcherFactory
						.createFCTHImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return fcthIS;
			}
		case Util.GABOR_NAME:
			if (null == gaborIS) {
				return ImageSearcherFactory
						.createGaborImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return gaborIS;
			}
		case Util.HASHING_CEDD_NAME:
			if (null == hceddIS) {
				return ImageSearcherFactory
						.createHashingCEDDImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return hceddIS;
			}
		case Util.JCD_NAME:
			if (null == jcdIS) {
				return ImageSearcherFactory
						.createJCDImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return jcdIS;
			}
		case Util.JCH_NAME:
			if (null == jchIS) {
				return ImageSearcherFactory
						.createJpegCoefficientHistogramImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return jchIS;
			}
		case Util.JH_NAME:
			if (null == jhIS) {
				return ImageSearcherFactory
						.createJointHistogramImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return jhIS;
			}
		case Util.LL_NAME:
			if (null == llIS) {
				return ImageSearcherFactory
						.createLuminanceLayoutImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return llIS;
			}
		case Util.OH_NAME:
			if (null == ohIS) {
				return ImageSearcherFactory
						.createOpponentHistogramSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return ohIS;
			}
		case Util.PHOG_NAME:
			if (null == phogIS) {
				return ImageSearcherFactory
						.createPHOGImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return phogIS;
			}
		case Util.SCD_NAME:
			if (null == scdIS) {
				return ImageSearcherFactory
						.createScalableColorImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return scdIS;
			}
		case Util.TAMURA_NAME:
			if (null == tamuraIS) {
				return ImageSearcherFactory
						.createTamuraImageSearcher(EACH_SEARCHER_RESULT_NUM);
			} else {
				return tamuraIS;
			}
		default:
			return null;
		}
	}

	/**
	 * 根据特征提取算法名获取对应的 DocumentBuilder
	 * 
	 * @param name
	 * @return
	 */
	public static DocumentBuilder getBuilderByAlgorithmName(String name) {
		switch (name) {
		case Util.CAC_NAME:
			return DocumentBuilderFactory
					.getAutoColorCorrelogramDocumentBuilder();
		case Util.CEDD_NAME:
			return DocumentBuilderFactory.getCEDDDocumentBuilder();
		case Util.CH_NAME:
			return DocumentBuilderFactory.getColorHistogramDocumentBuilder();
		case Util.CL_NAME:
			return DocumentBuilderFactory.getColorLayoutBuilder();
		case Util.EH_NAME:
			return DocumentBuilderFactory.getEdgeHistogramBuilder();
		case Util.FCTH_NAME:
			return DocumentBuilderFactory.getFCTHDocumentBuilder();
		case Util.GABOR_NAME:
			return DocumentBuilderFactory.getGaborDocumentBuilder();
		case Util.HASHING_CEDD_NAME:
			return DocumentBuilderFactory.getHashingCEDDDocumentBuilder();
		case Util.JCD_NAME:
			return DocumentBuilderFactory.getJCDDocumentBuilder();
		case Util.JCH_NAME:
			return DocumentBuilderFactory
					.getJpegCoefficientHistogramDocumentBuilder();
		case Util.JH_NAME:
			return DocumentBuilderFactory.getJointHistogramDocumentBuilder();
		case Util.LL_NAME:
			return DocumentBuilderFactory.getLuminanceLayoutDocumentBuilder();
		case Util.OH_NAME:
			return DocumentBuilderFactory.getOpponentHistogramDocumentBuilder();
		case Util.PHOG_NAME:
			return DocumentBuilderFactory.getPHOGDocumentBuilder();
		case Util.SCD_NAME:
			return DocumentBuilderFactory.getScalableColorBuilder();
		case Util.TAMURA_NAME:
			return DocumentBuilderFactory.getTamuraDocumentBuilder();
		default:
			return null;
		}
	}

	/**
	 * 判断文件是否为图片
	 * 
	 * @param file
	 * @return
	 */
	public static boolean isImage(File file) {
		String name = file.getName();
		// 获得文件后缀名
		String tmpName = name.substring(name.lastIndexOf(".") + 1,
				name.length());
		if (tmpName.equalsIgnoreCase("jpg") || tmpName.equalsIgnoreCase("jpeg")
				|| tmpName.equalsIgnoreCase("bmp")
				|| tmpName.equalsIgnoreCase("dib")
				|| tmpName.equalsIgnoreCase("gif")
				|| tmpName.equalsIgnoreCase("jfif")
				|| tmpName.equalsIgnoreCase("jpe")
				|| tmpName.equalsIgnoreCase("png")
				|| tmpName.equalsIgnoreCase("tif")
				|| tmpName.equalsIgnoreCase("tiff")
				|| tmpName.equalsIgnoreCase("ico")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 存储用户上传的图片文件
	 * 
	 * @param file
	 */
	/*public static void storeUploadFile(File file) {
		// 存储路径
		String storePath = UPLOAD_PATH + getDate() + File.separator;
		storePath += file.getName();
		File storeFile = new File(storePath);
		if (storeFile.exists()) {
			return;
		}
		try {
			if (storeFile.createNewFile()) {
				// 存储文件
				byte[] bytes = new byte[1024];
				InputStream is = new FileInputStream(file);
				OutputStream os = new FileOutputStream(storeFile);
				while (is.read(bytes) != -1) {
					os.write(bytes);
				}
				is.close();
				os.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/

	/** 排序算法名 */
	public static String sortName = null;
	/** 排序算法配置文件名 */
	public final static String SORT_CONFIG_NAME = "sortConfig.properties";
	/** 排名排序算法名 */
	public final static String RankSortName = "rank";
	/** 比例排序算法名 */
	public final static String RatioSortName = "ratio";

	public static String getSortName() {
		if (null == sortName) {
			// 读取基础配置文件
			InputStream inputStream = new Util().getClass().getClassLoader()
					.getResourceAsStream(SORT_CONFIG_NAME);
			// 加载成配置对象
			Properties p = new Properties();
			try {
				p.load(inputStream);
				// 获取配置信息
				Set<Object> keys = p.keySet();
				for (Iterator it = keys.iterator(); it.hasNext();) {
					String key = (String) it.next();
					String value = p.getProperty(key);
					if (null != value && value.equalsIgnoreCase("true")) {
						return key;
					}
				}
				return null;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return sortName;
	}

}
