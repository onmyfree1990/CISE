package com.cise.demo;

import com.cise.lire.Indexer.Indexer;

/**
 * 本引擎建议使用 Indexer 进行索引<br>
 * Indexer 是以 LIRE 框架中提供的索引器为基础实现的并行索引器<br>
 * LIRE 框架也有提供并行索引器的实现<br>
 * 但其生成的索引文件并没有根据特征提取算法分开<br>
 * 而本引擎所实现的并行索引器是将索引文件分开来的<br>
 * 一个特征提取算法对应一组索引文件，便于并行搜索器的搜索，以此提高搜索效率
 * @author Mutear
 *
 */
public class IndexerDemo {

	public static void main(String[] args) {
		/*
		 * 若要对现有的整个图片库进行索引，则在配置文件中配置图片库地址
		 * 然后直接调用 Indexer.start() 方法，进行索引
		 * */
		//Indexer.start();
		/*
		 * 若图片库中的部分图片已生成索引了，现需对其他图片进行索引
		 * 则调用 Indexer.start(sufPath) 方法
		 * 参数为基于已配置的图片库地址的后缀地址
		 * 如要进行索引的目录为 C:\pictures\test ,配置的图片库地址为 C:\pictures\
		 * 则 sufPath = "test";
		 * 此方法是为了满足图片库逐日增加图片的需求
		 * 如项目的图片库是由爬虫所得，爬虫每日都进行爬取，则可将后缀地址设置为日期
		 * 如2015年7月16日爬取了部分图片，则需对 C:\pictures\20150716 目录下的图片进行索引
		 * 此时只需调用 Indexer.start("20150716");
		 * */
		Indexer.start("20151018");
	}

}
