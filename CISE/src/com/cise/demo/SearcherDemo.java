package com.cise.demo;

import java.io.File;
import java.util.ArrayList;

import com.cise.lire.ParallelSearcher.utils.Result;
import com.cise.services.impl.SearchServiceImpl;

/**
 * 本引擎暂时只提供并行搜索器的实现<br>
 * 虽 LIRE 框架由提供单特征提取算法的搜索器的实现 <br>
 * 但本引擎暂未封装 <br>
 * 所以如果要使用单特征的搜索器，可对配置文件进行配置
 * @author Mutear
 *
 */
public class SearcherDemo {

	public static void main(String[] args) {
		// 输入文件，即要进行搜索的图片文件
		File file = new File("H:\\Nginx\\nginx-1.7.6\\nginx-1.7.6\\html\\pictures\\Scenery\\20151019\\pp_2E163_2Ecom\\grox\\pp\\12966086_2Ehtml\\8.jpg");
		SearchServiceImpl searchServiceImpl = new SearchServiceImpl(file);
		ArrayList<Result> results = searchServiceImpl.search();
		System.out.println(results.get(0).getDistance());
	}

}
