package com.cise.services;

import java.util.ArrayList;

import com.cise.lire.ParallelSearcher.utils.IResult;
import com.cise.lire.ParallelSearcher.utils.Result;

public interface SearchService {
	public ArrayList<Result> search();
}
