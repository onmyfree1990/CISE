package com.cise.services.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import com.cise.lire.ParallelSearcher.AbstractParallelSearcher;
import com.cise.lire.ParallelSearcher.ParallelSearcherFactory;
import com.cise.lire.ParallelSearcher.utils.IResult;
import com.cise.lire.ParallelSearcher.utils.Result;
import com.cise.services.SearchService;
import com.cise.utils.Util;

import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.ImageSearcherFactory;
import net.semanticmetadata.lire.SearchHitsFilter;

public class SearchServiceImpl implements SearchService {
	
	/**
	 * 用户上传的图片文件
	 */
	private File file = null;

	public SearchServiceImpl(File file) {
		this.file = file;
	}
	
	@Override
	public ArrayList<Result> search() {
		// file 为空或 file 不存在或 file 不是图片，则返回空
		if(null == file || !file.exists() || !Util.isImage(file)){
			return null;
		}
		
		// 构建并行搜索器
		AbstractParallelSearcher aps = ParallelSearcherFactory.
				createParallelSearcher(Util.SELECTED_ALGORITHMS);
		
		BufferedImage img = null;
		try {
			// 将图片文件转换格式
			img = ImageIO.read(file);
			
			// 搜索
			ArrayList<IResult> results = aps.search(img);
			
			// 搜索结果格式转换
			if(null == results || results.size() == 0) return null;
			ArrayList<Result> result = new ArrayList<Result>();
			for(IResult iResult : results){
				Result res = (Result) iResult;
				result.add(res);
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
